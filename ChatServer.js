const express = require('express');
const path = require('path');
const http = require('http');
const app = express();

// HTTP Server
const server = http.createServer(app);
const port = process.env.PORT || 8080;
server.listen(port);
console.log(`Express HTTP Server is listening at port ${port}`);
app.get('/', (request, response) => {
	// Bootstrap, JQuery, and local CSS
	app.use('/main.js', express.static(__dirname + '/main.js'));
	app.use('/images', express.static(__dirname + '/images'));
    app.use('/snake', express.static(__dirname + '/snake'));
    app.use('/css', express.static(__dirname + '/css'));
    app.use('/js', express.static(__dirname + '/js'));
	app.use('/css', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/css')));
    app.use('/js', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/js')));
    app.use('/js', express.static(path.join(__dirname, 'node_modules/jquery/dist')));
    app.use('/js', express.static(path.join(__dirname, 'node_modules/intro.js')));
    app.use('/js', express.static(path.join(__dirname, 'node_modules/autocompleter')));

	console.log("Got an HTTP request");
	response.sendFile(__dirname+'/index.html');
});

// Socket IO
const socketio = require('socket.io')(server);
console.log("Socket.IO is listening at port: " + port);

// Pass in socket.io to snake server file
const snakeGame = require('./snake/snakeServer.js');
//snakeGame.startSnakeGame();
// [[groupname]: {name: string, seen: boolean}}];
const MessageSeenStorage = {};

// {[groupname]: [username: string;]}
const userList = {};
const nullFriend = null;
const main_group_chat = "public";

socketio.on('connection', async (socketclient) => {
	console.log("A new Socket.IO client is connected. ID=" + socketclient.id);
	socketclient.emit('connection', await GetUserList(main_group_chat));

    // Init Snake Library
    snakeGame.startSnakeGame(socketio, socketclient);
	socketclient.on('disconnect', () => {
		console.log('A Socket.IO client has disconnected. ID=' + socketclient.id);
		CheckRemainingAuthenticatedClients(socketclient);
	});

	socketclient.on('login', async (username, password) => {
		console.log("DEBUG> Got username=" + username + ";password=" + password);
        var checklogin = await DataLayer.checklogin(username, password);
		if (checklogin) {
			AddAuthenticatedUser(main_group_chat, username).then(async () => {
				socketclient.authenticated = true;
				socketclient.username = username;
				socketclient.emit("authenticated", true, username, await GetFriendList(username), await GetAccountInfo(username));
				socketclient.join("user:"+username);
				const welcomemessage = username + " has join the chat system!";
				console.log(welcomemessage);
				socketio.sockets.emit("welcome", welcomemessage, await GetUserList(main_group_chat), await GetGroupList(username));
                
                var chat_history = await envimessengerdb.loadChatHistory("public", receiver="public");
                if (chat_history && chat_history.length > 0) {
                    chat_history = chat_history.reverse();
                    socketclient.emit("chat_history", chat_history, "public");
                }    
			});
		} else {
			socketclient.emit("authenticated", false, username);
		}
	});

	socketclient.on('message', (message) => {
		if(!socketclient.authenticated) {
			console.log("Unauthenticated client sent a message. Supress!");
			return;
		}
		if(!validate("message", message)) {
		    console.log("Invalid message send");
		    return;
        }
		console.log(socketclient.username + " says: " + message);
		socketio.emit("message", {type: "public", username: socketclient.username, message: message});
        envimessengerdb.storeChat("public", socketclient.username, "public", message);
		// 	SendToAuthenticatedClient(undefined, "message", {type: "public", username: socketclient.username, message: message});
		ClearSeenStatus('public-public');
	});

	socketclient.on('private-message', (username, message) => {
		if(!socketclient.authenticated) {
			console.log("Unauthenticated client sent a message. Supress!");
			return;
		}
        if(!validate("message", message)) {
            console.log("Invalid message send");
            return;
        }
		console.log(socketclient.username, "says to", username + ":", message);
        socketio.to('user:'+username).emit("message", {type: "private-receive", username: socketclient.username, message: message});
        // envimessengerdb.storeChat("private", socketclient.username, username, message);        
		if (socketclient.username !== username) {
			socketio.to('user:'+socketclient.username).emit("message", {type: "private-send", username: username, message: message});
            envimessengerdb.storeChat("private", socketclient.username, username, message);            
		}
	});

	socketclient.on('group-message', async (groupname, message) => {
		if(!socketclient.authenticated) {
			console.log("Unauthenticated client sent a message. Suppress!");
			return;
		}
        if(!validate("message", message)) {
            console.log("Invalid message send");
            return;
        }
		const groupMemberList = await GetUserList(groupname);
		console.log(groupMemberList);
		if (!groupMemberList.find(name => socketclient.username === name)) {
			console.log("Invalid client sent a message to group", groupname, "Suppress!");
			return;
		}
		console.log(socketclient.username, "says to group", groupname + ":", message);
        envimessengerdb.storeChat("group", socketclient.username, groupname, message);
		groupMemberList.forEach(username => {
			socketio.to('user:'+username).emit("message", {type: "group", username: socketclient.username, groupname: groupname, message: message});
		});
		ClearSeenStatus('group-'+groupname);
	});

	socketclient.on('logout', async () => {
		const logoutmessage = socketclient.username + " has logged out of socket " + socketclient.id + ".";
		socketclient.leave("user:" + socketclient.username);
		await CheckRemainingAuthenticatedClients(socketclient);
		delete socketclient.username;
		delete socketclient.authenticated;
		console.log(logoutmessage);
		socketclient.emit('logout', await GetFriendList(nullFriend));
		socketio.sockets.emit('connection', await GetUserList(main_group_chat));
	});

	socketclient.on('seen', (type, name) => {
		if (socketclient.authenticated && !CheckSeenStatus(main_group_chat, socketclient.username)) {
			if (type === 'private') {
				socketio.to('user:'+name).emit('seen', 'private', socketclient.username, socketclient.username);
			} else {
				const groupname = type + "-" + name;
				AddSeenStatus(groupname, socketclient).then((clients) => {
					socketio.sockets.emit('seen', type, name, clients.length);
				});
			}
		}
	});

    socketclient.on("<TYPE>", (type, name) => {
        if (socketclient.username == null) {return;}
        const username = socketclient.username;
        if (type === "private") {
			socketclient.emit("<TYPING>", username, type, name);
			socketio.to("user:"+name).emit("<TYPING>", username, type, socketclient.username);
		} else {
			socketio.sockets.emit("<TYPING>", username, type, name);
		}
        console.log(`[<TYPING> ${username}, ${type}, ${name}] is sent to all connected clients`);
    });

    socketclient.on("create-group", async (name, users) => {
        if (!validate("groupname", name)) {
            return;
        }
    	if (users) {
    		await users.forEach(user => {
				AddAuthenticatedUser(name, user).then(async () => {
					socketio.to("user:"+user).emit('connection', await GetUserList(main_group_chat), await GetGroupList(user));
				});
			});
		}
	});

    socketclient.on("register", async(username, password)=>{
        const registration_result = await DataLayer.addUser(username,password);
        socketclient.emit("registration", registration_result);
    });

    socketclient.on("chat_history", async (data) =>{
        var type = data.type;
        var name = data.name; //this is the group's name, receiver's name, or nothing
        if(type === "private"){
            var chat_history = await envimessengerdb.loadChatHistory(type, socketclient.username, name);
            var chat_history2 = await envimessengerdb.loadChatHistory(type, name, socketclient.username);
            chat_history = chat_history.concat(chat_history2);
            if (chat_history && chat_history.length > 0) {
                chat_history = chat_history.reverse();
                socketclient.emit("chat_history", chat_history, type, socketclient.username);
            }
        } else {
            var chat_history = await envimessengerdb.loadChatHistory(type, socketclient.username, name);
            if (chat_history && chat_history.length > 0) {
                chat_history = chat_history.reverse();
                socketclient.emit("chat_history", chat_history, type);
            }
        }
    });
    
    socketclient.on("friendRequest", (type, sender, friend) => {
        console.log("Debug> friendRequest: " + type + " " + sender + " " + friend);
        if (type === "isFriend") {
            console.log("ALREADY FRIENDS");
            socketclient.emit("alreadyFriendNotif", {senderUsername: sender, recieverUsername: friend});
        } else {
            socketclient.to('user:'+friend).emit("friendRequestNotif", {senderUsername: sender, recieverUsername: friend});
        }
    });

    socketclient.on("acceptFriendRequest", async(sender, reciever) => {
            await DataLayer.addFriendRelation(sender, reciever);
            await DataLayer.addFriendRelation(reciever, sender);
            //
            socketclient.to('user:'+sender).emit("updateFriendsList", await GetFriendList(sender));
            socketclient.emit("updateFriendsList", await GetFriendList(reciever));
    });

    socketclient.on("updateAccountInfo", async(username, fullName, phoneNum, email) => {
        await DataLayer.updateAccountInfo(username, fullName, phoneNum, email);
    });
});

var envimessengerdb=require("./envimessengerdb")
const DataLayer = {
	info: 'Data Layer Implementation for Messenger',
	async checklogin(username, password) {
		if (!validate("username", username) || password.length === 0) {
			console.log("checklogin: failure to validate ", username);
			return false;
		}
		console.log("checklogin: " + username + "/" + password);
		var result = await envimessengerdb.checklogin(username,password);
        console.log("Debug>DataLayer.checklogin->result="+ result);
        return result;
	},

	async addUser(username,password) {
		if (!validate("username", username) && !validate("password", password)) {
			console.log("addUser: failure to validate ", username, password);
			return "Error";
		}
        return await envimessengerdb.addUser(username,password);
    },
    
    async addFriendRelation(loggedInUser, friend) {
        // VALIDATION?
        return await envimessengerdb.addFriendRelation(loggedInUser, friend);
    },

    async getFriendsList(loggedInUser) {
        return await envimessengerdb.getFriendsList(loggedInUser);
    }, 
    
    async updateAccountInfo(username, fullName, phoneNum, email) {
        return await envimessengerdb.updateAccountInfo(username, fullName, phoneNum, email);
    },

    async getAccountInfo(username) {
        return await envimessengerdb.getAccountInfo(username);
    }
}

function SendToAuthenticatedClient(sendersocket, type, data) {
	var sockets = socketio.sockets.sockets;
	sockets.forEach((socketclient) => {
		if (socketclient.authenticated) {
			socketclient.emit(type, data);
			var logmsg = "DEBUG> sent msg to " + socketclient.username + " with ID=" + socketclient.id;
			console.log(logmsg);
		}
	});
}

function CheckRemainingAuthenticatedClients(client) {
	if (client.username) {
		const remaining_socket = socketio.of("/").adapter.rooms.get("user:"+client.username);
		if (!remaining_socket) {
			RemoveAuthenticatedUser(main_group_chat, client.username).then();
		}
	}
}

// Add authenticated user to userList if their username is not already on the list
async function AddAuthenticatedUser(groupname, username) {
	if(!userList[groupname]) {
		userList[groupname] = [];
	}
	if (!userList[groupname].find((u) => { return u === username; })) {
		console.log("Adding user", username, "to group", groupname);
		userList[groupname].push(username);
	}
}

async function RemoveAuthenticatedUser(groupname, username) {
	if(userList[groupname]) {
		console.log("Removing user", username, "from group", groupname);
		userList[groupname].splice(userList[groupname].indexOf(username), 1);
	}
}

function GetGroupList(username) {
	if (!username) {
		return [];
	}

	let groupList = Object.keys(userList);
	groupList.splice(groupList.indexOf("public"), 1);
	groupList = groupList.filter(group => {
		console.log(group);
		return userList[group].find(name => username === name)
	});
	console.log(groupList);
	return groupList;
}

function GetUserList(groupname) {
	if (userList[groupname] && userList[groupname].length > 0) {
		return userList[groupname];
	}
	return [];
}

async function GetFriendList(loggedInUser) {
    friendsListDocs = await DataLayer.getFriendsList(loggedInUser);
    friendsList = [];
    friendsListDocs.forEach(doc => {
        friendsList.push(doc.friend);
    });
    return friendsList;
}

async function GetAccountInfo(username) {
    accountInfo = await DataLayer.getAccountInfo(username);
    
    if (accountInfo == null) {
        return ["", "", ""];
    }
    accInfo = [accountInfo.fullName, accountInfo.phoneNumber, accountInfo.email];
    
    return accInfo;
}

async function AddSeenStatus(groupname, socketclient) {
	if (!MessageSeenStorage[groupname]) {
		MessageSeenStorage[groupname] = [];
	}
	// TODO: add seen status for unique users only
	if (MessageSeenStorage[groupname].indexOf(socketclient.username) < 0) {
		MessageSeenStorage[groupname].push(socketclient.username);
	}
	console.log("DEBUG> AddSeenStatus.", socketclient.username, "added to", groupname);
	return MessageSeenStorage[groupname];
}

function GetSeenStatus(groupname) {
	if (MessageSeenStorage[groupname]) {
		return MessageSeenStorage[groupname].length;
	}
	return 0;
}

function CheckSeenStatus(groupname, username) {
	if (MessageSeenStorage[groupname]) {
		return MessageSeenStorage[groupname].find((u) => { return u === username; });
	}
	return false;
}

async function ClearSeenStatus(groupname) {
	MessageSeenStorage[groupname] = [];
}

function validate(type, value) {
    if (type === "username") {
        return (value && value.length >= 4 && value.length <= 100 && /^[a-zA-Z0-9]+$/.test(value));
    } else if (type === "password") {
        return /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value);
    } else if (type === "groupname") {
        return (value && value.length >= 4 && value.length <= 100 && /^[a-zA-Z0-9]+$/.test(value));
    }  else if (type === "message") {
        return (value && value.length >= 1 && value.length <= 500);
    }
}

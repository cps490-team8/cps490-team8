FROM node:14
WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install

COPY . .
RUN echo "Creating a Docker image by Team 8"

CMD ["npm", "start"]

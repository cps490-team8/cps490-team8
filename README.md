# README.md - CPS490 Team 8 Report

Source: <https://bitbucket.org/cps490-team8/cps490-team8/src/master/README.md>

---
University of Dayton

Department of Computer Science

CPS 490 - Capstone I, Fall 2021

Instructor: Dr. Phu Phung

---

## Capstone I Project 

# ENVI Messenger Application

# Team members

1.  Wesam Haddad, haddadw1@udayton.edu
2.  Quan Nguyen, nguyenq2@udayton.edu
3.  Shara Shrestha, shresthas6@udayton.edu
4.  Cole Weslow, weslowc1@udayton.edu

# Project Management Information

Demo Link: https://envi-messenger-team8.herokuapp.com/

Management board (private access): <https://trello.com/b/Y1e8SVYe/the-messenger-project>

Source code repository (private access): <https://bitbucket.org/cps490-team8/cps490-team8/src/master/>


## Revision History

| Date     |   Version     |  Description |
|----------|:-------------:|-------------:|
|10/28/2021|  0.3		   | Sprint 2     |
|10/03/2021|  0.2		   | Sprint 1 	  |
|09/08/2021|  0.1b         | Init p2      |
|09/02/2021|  0.1a         | Init draft   |

# Overview

The Messenger Application shall create an easier way to communicate with others from various distances. The Messenger will have various fun themes, audio and normal messaging capabilities, and fun chat games to play with others.

# System Analysis

We will be using Node.js to develope the chat server and application.
![Analysis Figure](images/architecture-figure.png "Analysis Figure")

## User Requirements

The Messenger Application will allow the user to do the following:

- Send and receive messages from a single person or a group of people.
- See if a message was seen by others.
- View message history in group or singular settings.
- Use profile pictures.
- Have a login for their accounts to keep them secure.
- Change background themes to dark, light, or another fun theme.
- Play chat games with their friends.
- Send audio files to others.
- Send their location.

## Use cases

### Overview diagram
![Use Case Diagram](images/use-case-diagramSprint2.png)
#### Register New Account
 * Actor: Unregistered User
 * User Story: Unregistered users can create a new account
 * Use Case Description: Any unregistered users can create a new account. The username of the new account should be unique to that account. The password should be enforced to have at least 1 uppercase, 1 symbol, and at least 6 characters in length.
 
#### Receive Messages from User
 * Actor: Registered User and Unregistered User
 * User Story: Registered users can receive messages from any users
 * Use Case Description: Any user should be able to view messages send from another user in channels that they have sufficient permissions to. Messages received should be free of XSS. Messages can also be filtered for profanity. 
 
#### Login / Logout Functionality
 * Actor: Registered User
 * User Story: Registered users can login and logout of the application
 * Use Case Description: Any user should be able to login using correct username and password. Logged in user should be able to logout of the application. There should be sessions implemented to keep users logged in until they logout.

#### Send Messages to User
 * Actor: Registered User
 * User Story: Registered users can send messages to other users
 * Use Case Description: Any user should be able to send text messages to other users. Text messages should have a limit of 300 words per message.
 
#### Upload Profile Picture
 * Actor: Registered User
 * User Story: Registered users can upload profile picture
 * Use Case Description: Any registered user should be able to upload any screenshots in format such as .jpg, .png, etc. The image size can be limit to 10MB. Profile picture should be stored on a file server. The path to the image should be store along with user information in the database.
 
 #### Send Embedded Youtube Video
 * Actor: Registered User
 * User Story: Registered users can send a youtube link that can be played
 * Use Case Description: Any registered user can send a youtube link as a message in the chat and the system will detect if it is a youtube link and return 
 an Iframe to embed the video in the chat for other registered users to play

#### Create Groupchats
 * Actor: Registered User
 * User Story: Registered users can create private groupchats
 * Use Case Description: Any registered user should be able create private groupchats consisting of at least 2 or more registered users. Groupchats should have similar functionality to a regular chat. Only users within a groupchat can view and send messages to that groupchat.
 
#### Choose Application Themes
 * Actor: Registered User
 * User Story: Registered users can choose preset themes
 * Use Case Description: Any registered user should be able to choose a theme from a list of default themes. Themes can have different text colors, background colors, etc. Chosen theme should be selected by default the next time the registered user login.
 
#### Send Audio Messages
 * Actor: Registered User
 * User Story: Registered users can send audio files
 * Use Case Description: Any registered user should be able to send audio files as messages. Audio file can be limit to 10MB. Any users should be able to view this file if they have sufficient permissions to the chat channel.
 
#### Share Location
 * Actor: Registered User
 * User Story: Registered users can share their current location
 * Use Case Description: Any registered user should be able share their current location. Any users can view this location if they have sufficient permissions to do so. Clicking on the location message will open a map at that location through a 3rd-party application.
 
#### Play Mini-Games with Other Users
 * Actor: Registered User
 * User Story: Registered users can play minigames with other users
 * Use Case Description: Any registered user should be able host or join minigames created by another registered user. Other registered users can also spectate an ongoing minigame.
 
 
# System Design

## Use-Case Realization

![Sequence Diagram](https://media.discordapp.net/attachments/882356965022515274/894396215230345217/jLPDRzim3BtxLt2vrC6I_e2WA5fqM3kWwu05k-sYoaoYLfOCIquM3FlleoGxaHsv7mEMGo9BV8_a8oddobXMswQKsGUxmHAXaaoe.png)
![Sequence Diagram 2](images/createNewGameUML.png)
![Sequence Diagram 3](images/sendMessageEmbeddedYoutubeLink.png)


## Database 

_(Start from Sprint 3, keep updating)_

## User Interface

![Messenger Mockup](images/UI/messengerMockup.png "Messenger Mockup")

* This is the initial mockup of the messenger app

![Messenger User Interface](images/UI/messengerUI.PNG "Messenger User Interface")

* This is the current working version of the UI that the user is presented with when opening the messenger

## User guide/Demo

![Login](images/UI/messengerLogin.PNG "Messenger Login")    

* Users can input thier usernames and passwords here to begin chatting with other registered users

![Chat UI](images/UI/messengerChatUI.PNG "Messenger Chat UI")    

* Once logged in each online user will appear over in the "All Users" list and users can start chatting in the chatbox below.
    * Also all groups each user is a part of will be listed under the "All Groups" list 

![Private and Group Chat UI](images/UI/messengerPrivateGroupChat.PNG "Private and Group Chat UI") 

* There are 3 different chat options to select from     
	* Public: This sends to all users regardless of login status     
	* Private: This prompts the user to type out who they want to send a message to using an autocomplete dropdown and only that user can see the message     
	* Group: This prompts the user to type out which group they want to send a message to using an autocomplete dropdown and only users in that group will see the message     

![Create Group Chat Modal](images/UI/createGroupModal.PNG "Create Group Modal")
![Create Group Chat](images/UI/createGroupButton.PNG "Create Group Button")     

* To create a new groupchat click on the "Create New Group" button, this will open a prompt for creating a new group chat
    * Check all users you want to include in the groupchat, and give the new group a name. Then hit create.

![Helper Display](images/UI/messengerHelperDisplay.PNG "Helper Display")
![Helper Display Button](images/UI/messengerHelperButton.PNG "Helper Button")   

* To get a tour of the site to see all the features click on the helper button at the bottom of the application

# Implementation

_(Start from Sprint 1, keep updating)_

For each new sprint cycle, update the implementation of your system (break it down into subsections). It is helpful if you can include some code snippets to illustrate the implementation

Specify the development approach of your team, including programming languages, database, development, testing, and deployment environments. 

### Sprint 2

## Embedded Youtube Videos

![Embedded Youtube 1](images/code/embedYoutube1.PNG)     
![Embedded Youtube 2](images/code/embedYoutube2.PNG)     
![Embedded Youtube 3](images/code/embedYoutube3.PNG)     

Once the message is recieved it uses the regular expression to check if the sent message was a youtube link or not. If it is a match, the youtube video ID is extracted and creates a new embeded link. If a match is found then it creates an Iframe with the youtube link as the source in the chatbox.     

## Snake Game Implementation

![Snake Game 1](images/code/snake1.PNG)    
When creating a new game it creates a socketio room using a randomly generated roomCode    
![Snake Game 2](images/code/snake2.PNG)   
When joining a game the server takes in the inputed room code and finds a matching socketio room that is already created to join after two players have entered  


### Sprint 1
ENVi Messenger Application currently contain two parts. The front-end features a user-friendly User Inferface (UI) developed in HTML/JS. The UI uses Bootstrap for majority of its components, along with other libaries such as JQuery, Intro.js, and Autocomplete. The front-end UI is reponsible for displaying data coming from the back-end service, along with sending requests to the back-end service. The back-end service uses a robust Node.js web application framework, Express. It features real-time, non-blocking asynchronous API, allowing for faster response time. The application is deployed on Heroku. 

### User Log in
All users information are stored in our MongoDB database. User information uses the format `{username: string; password: string}`. When a user log in or register an account, the information entered will be validated using the following validate function. This validation is done at the UI, backend, and database layer.
```js
function validate(type, value) {
	if (type === "username") {
		return (value && value.length >= 4 && value.length <= 100);
	} else if (type === "password") {
		return /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value);
	}
}
```
Once the username and password had been validated, the information is send to the backend, then to the DataLayer to check if that account is in the database.
```
const checklogin = async (username,password) => {
    if (!validate("username", username) || password.length === 0) {
        return false;
    }
    var users = getDb().collection("Users");
    var user = await users.findOne({username:username,password:password});
    if(user!=null && user.username==username && user.password==password){
        return true;
    }
    return false;
}
```
The information receieved from the DataLayer is eventually return back to the front-end to handle.

### User Registeration
The new user registeration also follow the user information format from "User Log in". Similar to the log in implementation, upon hitting the register button, the information entered will also be validated at all layers and sent to the backend to process.
The DataLayer checks for existing username, then create a new username/password entry on the database.
```
const addUser = async (username,password) => {
	// validation
	....
	var users = getDb().collection("Users");
	var user = await users.findOne({username:username});
	if(user!=null && user.username==username){
		return "UserExists";
	} else {
		...
		const newUser = {"username": username,"password" : password}
		const result = await users.insertOne(newUser);
		...
	}
}
```

### Chat Messenges
All chat messages follows the format `{type: string; name: string}`. Where `type` can be `public`, `private`, or `group`. The `name` represent the username or groupname; this will eventually be change to `id`.
All request will be validated to ensure user is logged in.

#### Public Chat
When a user hit the send button in the UI. The front-end send a message to the backend. `socketio.emit('message', message);`. The backend will validate the user, then send the message to all clients.
```js
socketclient.on('message', (message) => {
	if(!socketclient.authenticated) {
		return;
	}
	socketio.emit("message", {type: "public", username: socketclient.username, message: message});
});
```
The front-end listen for "message" and handles it accordingly.
```js
if (type === "public") {
	if (!match) {
		// write message to the UI
	}
}
```

#### 
	
## Deployment

Describe how to deploy your system in a specific platform.

# Software Process Management

We use Scrum management process. Our project branch follows these rules: 
- master branch contains fully tested version of the application
- develop branch contains the most up-to-date code
- feature folder for new features
- bugfix folder for bug fixes

For each task, new branch is create for implementation under the feature or bugfix folder. When a task is ready, a pull request is required for the implementation to be merged into the develop branch. At the end of each sprint, fully tested code in the develop branch are merged into master branch.

We use Trello our management board. A card is created for each task. Each card contains estimated date, estimated time, time logged, link to the relevant branch and pull requests for the task. Cards are organized in term of their priority; more needed or important tasks will on top.

The board is divided into 5 categories:
- Product Backlog
	- Use cases and features for future sprints
- Sprint Backlog
	- All tasks for the current sprint
- In Progress
	- Tasks that are being work on
- In Review
	- Finished tasks and pull requests ready to be review
- Completed
	- Finished tasks that are merged into develop branch
	
We have daily stand-up meetings. In each meeting, we record each member contributions since last meeting and ensure each member have a task to work on. In these meeting, we also update our Trello board to ensure everything is up-to-date.

At the end of each sprint, we have a sprint review where we go over completed tasks for the sprint, record member contributions, and complete the sprint retropsective. This is recorded under "Scrum process."

#### Current Sprint backlog

![Sprint 3 backlog](https://cdn.discordapp.com/attachments/882356965022515272/903391122389352448/sprint3-trello-board.PNG)

#### Gantt Chart

![Gantt Chart](https://cdn.discordapp.com/attachments/882356965022515272/903391497771163648/sprint3-gantt-chart.PNG)

## Scrum process

### Sprint 2

Duration: 10/05/2021-10/28/2021

#### Completed Tasks: 

1. Improve UI Design
2. Seen Status for Groupchats 
3. Seperate Chat UI for Group and Private Chat
4. Database Storage
5. Register Account
6. Multiplayer Snake Game *
7. Embedded Youtube Videos*
8. Data Validation for Login
9. 'ENTER' to send message and clear data

\* extra features

#### Contributions: 

1.  Wesam Haddad, 24 hours
2.  Quan Nguyen, 30 hours
3.  Shara Shrestha, 23 hours
4.  Cole Weslow, 23 hours

#### Sprint Retrospective:
| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
| Completing Tasks | Time Management | Understand Sprint requirments |
| Bitbucket PR and Branches | Delegating tasks | Allocate tasks based on skill level and interest | 
| Keeping Trello up to date | Managing Ambitions | Stay within Scope |
| Met all the requirments |

### Sprint 1

Duration: 09/10/2021-10/04/2021

#### Completed Tasks: 

1. Design UI layout
2. Set-up Bootstrap 
3. Implement UI elements
4. Set-up Heroku/cloud elements
5. Send and receive messages
6. Private messaging 
7. Login functionality *
8. Logout functionality *
9. Help functions *
10. Display type status 
11. Create Groupchat *
12. Message Seen Status *
13. Group chat functionality *
14. Autocomplete Feature *
15. Basic Security Measures (Data Validation, Data Races) *

\* extra features

#### Contributions: 

1.  Wesam Haddad, 19 hours
2.  Quan Nguyen, 21 hours
3.  Shara Shrestha, 17 hours
4.  Cole Weslow, 15 hours

#### Sprint Retrospective:
| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
| Daily Updates | Time Management | Understand Sprint requirments |
| Bitbucket PR and Branches | Delegating tasks | Allocate tasks based on skill level and interest |
| Keeping Trello up to date | 
| Met all the requirments |

### Sprint 0

Duration: 08/26/2021-09/09/2021

#### Completed Tasks: 

1. Meeting Time
2. User Requirements
3. Use Case Diagram
4. Sprint 0 Presentation

#### Contributions: 

1.  Wesam Haddad, 3 hours, contributed in Trello setup and sprint 0 presentation
2.  Quan Nguyen, 3 hours, contributed in system analysis, use cases
3.  Shara Shrestha, 3 hours, contributed in project set up, use cases, presentation
4.  Cole Weslow, 3 hours, contributed in overview, user requirements, Gantt Chart, Sprint 0 Presentation 

#### Sprint Retrospective:
| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
| Met all goals for Sprint 0 | Division of task | Update and check Trello board
| Everyone present and on time for daily standup



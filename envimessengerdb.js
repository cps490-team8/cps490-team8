const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://weslowc1:EnviMessengerTeam8@envidb.msrgi.mongodb.net/Envi-Messenger?retryWrites=true&w=majority";
const mongodbclient = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
const bcrypt = require("bcrypt");
let db = null;
mongodbclient.connect( (err,connection) => {
    if(err) throw err;
    console.log("Connected to the MongoDB cluster!");
    db = connection.db();
})
const dbIsReady = ()=>{
    return db != null;
};
const getDb = () =>{
    if(!dbIsReady())
        throw Error("No database connection");
    return db;
}

const addUser = async (username,password) => {
    if (!validate("username", username) && !validate("password", password)) {
        console.log("addUser: failure to validate ", username, password);
        return "Error";
    }

    var users = getDb().collection("Users");
    var user = await users.findOne({username:username});
    if(user!=null && user.username==username){
        console.log(`Debug>messengerdb.addUser: Username '${username}' exists!`);
        return "UserExists";
    } else {
        const salt = await bcrypt.genSalt(10);
        password = await bcrypt.hash(password, salt);
        const newUser = {"username": username,"password" : password}
        try{
            const result = await users.insertOne(newUser);
            if(result!=null){
                console.log("Debug>messengerdb.addUser: a new user added: \n", result);
                return "Success";
            }
        } catch {
            console.log("Debug>messengerdb.addUser: error for adding '" +
            username +"':\n", err);
            return "Error";
        }
    }
}

const addFriendRelation = async (loggedInUser, friend) => {
    var friendsList = getDb().collection("friends");
    if (loggedInUser == friend) {
        console.log(`Debug>messengerdb.addFriendRelation: Can't add '${friend}' as friend!`);
    } else {
        const newFriendLink = {"loggedInUser": loggedInUser, "friend": friend}
        try {
            const result = await friendsList.insertOne(newFriendLink);
            if(result != null) {
                console.log("Debug>messengerdb.addFriendRelation: New friendship made");
            }
        } catch {
            console.log("Debug>messengerdb.addFriendRelation: Error in making friendship");
        }
    }
    

}

const updateAccountInfo = async (username, fullName, phoneNum, email) => {
    var userAccountInfoList = getDb().collection("user-account-info");
    console.log();
    if(await userAccountInfoList.find({username: username}).count() == 0) { // No previous account info
        const newAccountInfo = {"username":username, "fullName":fullName, "phoneNumber":phoneNum, "email":email};
        await userAccountInfoList.insertOne(newAccountInfo);
    } else { // just update existing info
        var query = {username:username};
        var newInfo = { $set: {fullName:fullName, phoneNumber:phoneNum, email:email}};
        await userAccountInfoList.updateOne(query, newInfo);
    }
}

const getAccountInfo = async (username) => {
    var allAccountInfo = getDb().collection("user-account-info");
    return await allAccountInfo.findOne({username:username});
}

const getFriendsList = async (loggedInUser) => {
    if (loggedInUser == null) return [];
    var query = { loggedInUser: loggedInUser};
    var friendName = {friend: 1, _id: 0};
    var friendsList = getDb().collection("friends");

    list = await friendsList.find(query,friendName).toArray();
    return list;
}

const checklogin = async (username,password) => {
    if (!validate("username", username) || password.length === 0) {
        console.log("checklogin: failure to validate ", username);
        return false;
    }
    var users = getDb().collection("Users");

    var user = await users.findOne({username:username});
    // Check if password matched stored hashed password in database
    const validPassword = await bcrypt.compare(password, user.password);
    console.log("Debug>messengerdb.checkLoginValidPassword-> " + validPassword);
    if(user!=null && user.username==username && validPassword){
        console.log("Debug>messengerdb.checklogin->user found:\n" + JSON.stringify(user));
        return true;
    }
    return false;
}


function validate(type, value) {
    if (type === "username") {
        return (value && value.length >= 4 && value.length <= 100 && /^[a-zA-Z0-9]+$/.test(value));
    } else if (type === "password") {
        return /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value);
    } else if (type === "groupname") {
        return (value && value.length >= 4 && value.length <= 100 && /^[a-zA-Z0-9]+$/.test(value));
    }  else if (type === "message") {
        return (value && value.length >= 1 && value.length <= 500);
    }
}

const storeChat = (type, sender, receiver, message)=>{
    console.log("DEBUG>> Storing Message to MongoDB");
    let timestamp = Date.now();
    let chat = {sender:sender, receiver: receiver, message:message, timestamp:timestamp};
    try{
        const inserted = getDb().collection(type).insertOne(chat);
        if(inserted != null){
            console.log("Debug>messengerdb.storeChat: a new chat message added: \n", JSON.stringify(chat));
        }
    }  catch {
        console.log("Debug>messengerdb.addUser: error for adding '" + JSON.stringigy(chat) + "'\n");
    }
}

const loadChatHistory = async (type, sender, receiver, limits=100)=>{
    if(type === "private"){
        var chat_history = await getDb().collection(type).find({sender:sender, receiver:receiver}).sort({timestamp:-1}).limit(limits).toArray();
        //console.log(chat_history);
    } else {
        var chat_history = await getDb().collection(type).find({receiver:receiver}).sort({timestamp:-1}).limit(limits).toArray();
        //console.log(chat_history);
    }
    
    if(chat_history && chat_history.length > 0){
        return chat_history;
    }
}

const getUserList = async ( ) => {
    var users = getDb().collection("Users");
    var collection = [];
    
    collection = await users.find().map(function(doc) { return doc;}).toArray();

    return collection;
}

const getUserById = async (id) => {
    var ObjectId = require('mongodb').ObjectID;
    userList = await getUserList();
    console.log("USERLIST TYPE: " + typeof(userList));

    console.log("UserList[0]: "  + userList[0]._id);

    return userList.filter(
        function(userList) {
            return userList._id == id;
        }
    )
}

const addGroup = async (groupname, users) => {
    if (!validate("groupname", groupname)) {
        console.log("addGroup: failure to validate ", groupname);
        return "Error";
    }

    var groups = getDb().collection("Groups");
    users.forEach(async username => {
        var user = await users.findOne({username:username, groupname:groupname});
        if(user!=null && user.username==username){
            console.log(`Debug>messengerdb.addGroup: Username '${username}' already in group '${groupname}!`);
            return false;
        }  else {
            const newUser = {"username": username,"groupname" : groupname}
            try{
                const result = await groups.insertOne(newUser);
                if(result!=null){
                    console.log(`Debug>messengerdb.addGroup: a new user '${username}' added to group '${groupname}' \n`);
                }
            } catch {
                console.log(`Debug>messengerdb.addGroup: error for adding '${username}' added to group '${groupname}'\n`);
                return false;
            }
        }
    });
    return true;
}

const findGroups = async (username) => {
    var db = getDb().collection("Groups");
    var groups = await db.find({username: username}).toArray();
    console.log(groups);
    return groups;
}

module.exports = {checklogin,addUser,addFriendRelation,getFriendsList,updateAccountInfo,getAccountInfo,storeChat,loadChatHistory,addGroup,findGroups};

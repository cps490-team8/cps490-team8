function startSiteTutorial() {
    introJs().setOptions({
        steps: [{
            title: 'Welcome!',
            element: document.querySelector('#appLogo'),
            intro: 'Click on through to check out some cool features!'
        },
        {
            title: 'Sign In/Sign Out and Edit Info',
            element: document.querySelector('#accountBtn'),
            intro: 'Click here to login and chat with friends. Once you\'re done you can log out here! Also you can edit your account information'
        },
        {
            title: 'Chat List',
            element: document.querySelector('#chatsList'),
            intro: 'This is where you can see all users online, and also all your groupchats'
        },
        {
            title: 'Add a Friend',
            element: document.querySelector('#allUsers'),
            intro: 'Right Click a user to send them a friend request!'
        },
        {
            title: 'Friends List',
            element: document.querySelector('#allFriends'),
            intro: 'You can only talk with people online if they\'re friends with you!'
        },
        {
            title: 'Create a new Groupchat',
            element: document.querySelector('#createGroupBtn'),
            intro: 'Click this to create a new groupchat with your closer friends!'
        },
        {
            title: 'Play some Snake',
            element: document.querySelector('#newGameButton'),
            intro: 'Create a new game and send the game code in the chat to play with a friend!'
        },
        {
            title: 'Chat Box',
            element: document.querySelector('#messages'),
            intro: 'Where the magic happens! Here is where you can see all the messages of the chat you\'re currently in'
        },
        {
            title: 'Chat Group',
            element: document.querySelector('#currChat'),
            intro: 'This will show which groupchat you are sending messages to. It can be either to another user, a private groupchat, or the public forum chat!'
        },
        {
            title: 'Send a Message',
            element: document.querySelector('#sendMessageBox'),
            intro: 'Type here to start sending messages to friends!'
        },
        {
            title: 'Website Tutorial',
            element: document.querySelector('#helperButton'),
            intro: 'Need a refresher of what things do? Click here to find out!'
        }]
    }).start();
}

class Cookie {
    static get(name) {
        const cookieName = `${encodeURIComponent(name)}=`;
        const cookie = document.cookie;
        let value = null;

        const startIndex = cookie.indexOf(cookieName);
        if (startIndex > -1) {
            let endIndex = cookie.indexOf(';', startIndex);
            if (cookie == -1) {
                endIndex = cookie.length;
            }
            value = decodeURIComponent(cookie.substring(startIndex + name.length, endIndex));
        }
        return value;
    }

    static set(name, value, expires, path, domain, secure) {

        let cookieText = `${encodeURIComponent(name)}=${encodeURIComponent(value)}`;
        if (expires instanceof Date) {
            cookieText += `; expires=${expires.toGMTString()}`;
        }

        if (path) cookieText += `; path=${path}`;
        if (domain) cookieText += `; domain=${domain}`;
        if (secure) cookieText += `; secure`;

        document.cookie = cookieText;
    }

    static remove(name, path, domain, secure) {
        Cookie.set(name, "", new Date(0), path, domain, secure);
    }
}

if (Cookie.get('visited') == null) {
    startSiteTutorial();
    Cookie.set('visited', 'true');
} else {
    console.log("COOKIE MADE: " + Cookie.get('visited'));
}

function removeCookie(cname) {
    Cookie.remove(cname);
}
var socketio = io();
var userList = [];
var groupList = [];
var friendsList = [];
var autocompleteData = [];
var authenticatedUsername;
let currentChat = {type: 'public', name: 'public'};

const status = { authenticated: false, seen: {} };

// Youtube link regular expression
var youtubeRegExp = /^https?\:\/\/(?:www\.youtube(?:\-nocookie)?\.com\/|m\.youtube\.com\/|youtube\.com\/)?(?:ytscreeningroom\?vi?=|youtu\.be\/|vi?\/|user\/.+\/u\/\w{1,2}\/|embed\/|watch\?(?:.*\&)?vi?=|\&vi?=|\?(?:.*\&)?vi?=)([^#\&\?\n\/<>"']*)/i;
var youtubeIDRegExp = /(\?|&)v=([^&#]+)/;

// Right click Menu
let defaultContextmenu = document.defaultContextmenu;
document.oncontextmenu = rightClick;
document.onclick = hideMenu;

socketio.on('connection', (userList, groupList, friendsList) => {
    console.log("UserList:" + userList);
	updateUserList(userList);
	updateGroupList(groupList);
    updateFriendsList(friendsList);
});

socketio.on('welcome', (welcomemessage, userList, groupList, friendsList) => {
	document.getElementById('messages-public-public').innerHTML += "<p class='welcome'>" + sanitizeHTML(welcomemessage) + "</p><br/>";
	updateUserList(userList);
	updateGroupList(groupList);
});

socketio.on("chat_history",(chat_history, type, currentUser) => {
	// console.log("receiving chat history from type", type, currentUser);
    if(chat_history && chat_history.length > 0){
        for(let index in chat_history){            
            let chatmessage = chat_history[index].message;
            let chatsender = chat_history[index].sender;
            let chatreceiver = chat_history[index].receiver;
            let time = new Date(chat_history[index].timestamp);
            // console.log(chatsender, chatreceiver, authenticatedUsername, chatmessage);
            if (type == "public") {
                if (chatsender === authenticatedUsername) {
                    document.getElementById('messages-public-public').innerHTML += `<p class='from-me'>${chatmessage}</p>`;
                } else {
                    document.getElementById('messages-public-public').innerHTML += chatsender + "<p class='from-them'>" + chatmessage + "</p>";
                }
            } else if (type == "private") {
                if(chatreceiver === currentUser){
                    document.getElementById('messages-' + type + '-' + chatsender).innerHTML+= 	chatsender + "<p class='from-them'>" + chatmessage + "</p>";
                } else {
                    document.getElementById('messages-' + type + '-' + chatreceiver).innerHTML+= `<p class='from-me'>${chatmessage}</p>`;
                }
            } else if (type== "group") {
				if (chatsender === authenticatedUsername) {
					document.getElementById('messages-group-'+chatreceiver).innerHTML += `<p class='from-me'>${chatmessage}</p>`;
				} else {
					document.getElementById('messages-group-'+chatreceiver).innerHTML += chatsender + "<p class='from-them'>" + chatmessage + "</p>";
				}
			}
        }
    }
});

socketio.on("authenticated", (authenticated, username, friendsList, accountInfo) => {
    updateFriendsList(friendsList);
    retrieveAccountInfo(accountInfo);
	status.authenticated = authenticated;
	if (authenticated) {
		authenticatedUsername = username;
		username = sanitizeHTML(username);
		document.getElementById("password").classList.remove("is-invalid");        
		$('#loginModal').modal('hide');
		document.getElementById("accountBtn").innerHTML = "Logout";
		// document.getElementById("login-header").innerHTML = "Logged in as " + username + ".";

		document.getElementById("createGroupBtn").removeAttribute("disabled");
		document.getElementById("sendMessageControl").removeAttribute("disabled");
        document.getElementById("newGameButton").removeAttribute("disabled");
        document.getElementById("joinGameButton").removeAttribute("disabled");
		document.getElementById("message").placeholder = "Type Message...";
        // make new div in html, replace ID (below), instead of +=, toggle display on css to none, jquery!,
        //example somewhere, ctrl F ".show" 
		document.getElementById("messages-public-public").innerHTML += "<p class='welcome'> Welcome " + username + "!</p>";
        document.getElementById("accountUsername").innerHTML = username;
        socketio.emit("chat_history", {type:"public", name:"public"});
    } else {
		document.getElementById("password").classList.add("is-invalid");
		document.getElementById("login-error").innerHTML = "Invalid username or password.";

	}
});

socketio.on("logout", (friendsList) => {
    updateFriendsList(friendsList);
	authenticatedUsername = null;
	updateGroupList([]);
	document.getElementById("username").value = "";
	document.getElementById("password").value = "";
	document.getElementById("password").classList.remove("is-invalid");

	document.getElementById("accountBtn").innerHTML = "Login";
	document.getElementById("login-header").innerHTML = "";

	document.getElementById("createGroupBtn").setAttribute("disabled", "");
	document.getElementById("sendMessageControl").setAttribute("disabled", "");
	// document.getElementById("sendMessageName").value = "public";
	document.getElementById("message").value = "";
	document.getElementById("message").placeholder = "Please login to send messages...";
	document.getElementById("messages").innerHTML += "You have logged out.<br/>";
	document.getElementById("newGameButton").setAttribute("disabled", "");
	document.getElementById("joinGameButton").setAttribute("disabled", "");
});



socketio.on('message', (data) => {
    console.log(data);
	const type = data.type;
	const user = data.username;
	const message = data.message;
	const groupname = data.groupname;
    var match = message.match(youtubeRegExp);
    var IDmatch = message.match(youtubeIDRegExp);
    // console.log("ID: " + IDmatch[2]);
    var youtubeIFrame = null;
    if(match) {
        youtubeIFrame = document.createElement('iframe');
		youtubeIFrame.width= "560";
		youtubeIFrame.height="315";
        youtubeIFrame.allow = "fullscreen;";
        youtubeIFrame.src = "https://www.youtube.com/embed/" + IDmatch[2];
    }
	// console.log("DEBUG> incoming message", data);
	if (type === "public") {
		if (!match) {
            if (user == authenticatedUsername) { // I am the sender -> message on right
                document.getElementById('messages-public-public').innerHTML += "<p class='from-me'>" + message + "</p>";
            } else {
                document.getElementById('messages-public-public').innerHTML += user + "<p class='from-them'>" + message + "</p>";
            }
            document.getElementById('messages').scrollTop = document.getElementById('messages').scrollHeight;
			//document.getElementById('messages-public-public').innerHTML += user + ": " + message + "<br/>";
		} else {
            if (user == authenticatedUsername) { // I am the sender -> message on right
                document.getElementById('messages-public-public').innerHTML += "<p name='youtubeFromMe' class='from-me'></p>";
                var links = document.getElementsByName('youtubeFromMe')
                links[links.length-1].append(youtubeIFrame);
            } else {
                document.getElementById('messages-public-public').innerHTML += user + "<p name='youtubeFromThem' class='from-them'> </p>";
                var links = document.getElementsByName('youtubeFromThem');
                links[links.length-1].append(youtubeIFrame);
            }
            document.getElementById('messages').scrollTop = document.getElementById('messages').scrollHeight;
		}
		status.seen[`public-public`] = false;
		setSeenStatus("public", "public", 0);
	} else if (type === "private-send") {
		if (!checkChatWindow(type, user)) {
			return;
		}
		if (!match) {
			document.getElementById('messages-private-'+user).innerHTML += "<p class='from-me'>" + message + "</p>";;
            document.getElementById('messages').scrollTop = document.getElementById('messages').scrollHeight;
		} else {
            document.getElementById('messages-private-'+user).innerHTML += "<p name='youtubePrivateFromMe' class='from-me'></p>";
            var links = document.getElementsByName('youtubePrivateFromMe');
            links[links.length-1].append(youtubeIFrame);
            document.getElementById('messages').scrollTop = document.getElementById('messages').scrollHeight;
		}
		setSeenStatus("private", user, 0);
	} else if (type === "private-receive") {
		if (!checkChatWindow(type, user)) {
			return;
		}
		if (!match) {
			document.getElementById('messages-private-'+user).innerHTML += user + "<p class='from-them'>" + message + "</p>";
            document.getElementById('messages').scrollTop = document.getElementById('messages').scrollHeight;
		} else {
            document.getElementById('messages-private-'+user).innerHTML +=  user + "<p name='youtubePrivateFromThem' class='from-them'></p>";
            var links = document.getElementsByName('youtubePrivateFromThem');
            links[links.length-1].append(youtubeIFrame);
            document.getElementById('messages').scrollTop = document.getElementById('messages').scrollHeight;
		}		
        status.seen[`private-${user}`] = false;
		setSeenStatus("private", user, 0);
	} else if (type === "group") {
		if (!checkChatWindow(type, groupname)) {
			return;
		}
		status.seen[`${type}-${groupname}`] = false;
        if (!match) {
            if (user == authenticatedUsername) { // I am the sender -> message on right
                document.getElementById('messages-group-'+groupname).innerHTML += "<p class='from-me'>" + message + "</p>";
            } else {
                document.getElementById('messages-group-'+groupname).innerHTML += user + "<p class='from-them'>" + message + "</p>";
            }
			//document.getElementById('messages-group-'+groupname).innerHTML += user + ": " + message + "<br/>";
        } else {
            if (user == authenticatedUsername) { // I am the sender -> message on right
                document.getElementById('messages-group-'+groupname).innerHTML += "<p name='youtubeGroupFromMe' class='from-me'></p>";
                var links = document.getElementsByName('youtubeGroupFromMe');
                links[links.length-1].append(youtubeIFrame);
            } else {
                document.getElementById('messages-group-'+groupname).innerHTML += user + "<p name='youtubeGroupFromThem' class='from-them'> </p>";
                var links = document.getElementsByName('youtubeGroupFromThem');
                links[links.length-1].append(youtubeIFrame);
            }
            document.getElementById('messages').scrollTop = document.getElementById('messages').scrollHeight;
        }
	}
});


socketio.on('seen', (type, name, amount) => {
	setSeenStatus(type, name, amount);
});

socketio.on("<TYPING>", (username, type, name) => {
	const typingElement = document.getElementById(`messages.typing-${type}-${name}`);
	if (typingElement) {
		typingElement.innerHTML = "<i>" + sanitizeHTML(username) + " is typing...</i>";
		setTimeout(() => { typingElement.innerHTML = "<br>"; }, 500);
	}
    // document.getElementById("currentlyTyping").innerHTML = "<i>" + msg + " is typing<span>.</span> </i>";
    // setTimeout(function(){document.getElementById("currentlyTyping").innerHTML = "<br>";}, 3000);
    // console.log("<TYPING> Received from server: " + msg);
});

socketio.on("registration", (result)=> {
    const username = document.getElementById('newusername').value;
	document.getElementById('newusername').value="";
	document.getElementById('newpassword').value="";

    if(result === "Success"){
        alert("...You can login now!");
		document.getElementById("newusername").classList.remove("is-invalid");
		document.getElementById("newpassword").classList.remove("is-invalid");
		showRegistrationUI(false);
		return;
    }

	document.getElementById("newpassword").classList.add("is-invalid");
	const errorElement = document.getElementById("registration-error");

	if(result==="UserExist") {
		errorElement.innerHTML = "Username" + username + " exists. Please try again!";
    } else {
		errorElement.innerHTML = "User registration error. Please try again!";
    }

    document.getElementById('newusername').focus();
})

function accountBtn() {
	if (!authenticatedUsername) {
		$('#loginModal').modal('show');
	} else {
        $('#accountModal').modal('show');
		// logout();
	}
}

function login() {
	var username = document.getElementById('username').value;
	if (!validate("username", username)) {
		document.getElementById("username").classList.add("is-invalid");
		document.getElementById("login-error").innerHTML = "Invalid username. Must be at least 4 characters and contains no special characters.";
		return;
	}
	var password = document.getElementById('password').value;
	if (!password || password.length === 0) {
		document.getElementById("password").classList.add("is-invalid");
		document.getElementById("login-error").innerHTML = "Password cannot be empty.";
		return;
	}

	if(socketio) {
		socketio.emit('login', username, password);
	}
}

function logout() {
	if(socketio) {
		socketio.emit('logout');
        $('#accountModal').modal('hide');
	}
}

function editAccountInfo() {
    fullName = document.getElementById("fullName");
    phoneNumber = document.getElementById("phoneNumber");
    email = document.getElementById("email");

    fullName.removeAttribute("readonly");
    phoneNumber.removeAttribute("readonly");
    email.removeAttribute("readonly");
}

function updateAccountInfo() {
    fullName = document.getElementById("fullName");
    phoneNumber = document.getElementById("phoneNumber");
    email = document.getElementById("email");
    
    fullName.setAttribute("readonly","true");
    phoneNumber.setAttribute("readonly","true");
    email.setAttribute("readonly","true");

    if (socketio) {
        socketio.emit("updateAccountInfo", authenticatedUsername, fullName.value, phoneNumber.value, email.value);
    }
}

function retrieveAccountInfo(accountInfo) { // Get info from database
    document.getElementById("fullName").value = accountInfo[0];
    document.getElementById("phoneNumber").value = accountInfo[1];
    document.getElementById("email").value = accountInfo[2];
}

function sendMessage() {
	const type = currentChat.type;
	const message = sanitizeHTML(document.getElementById('message').value);
	if (!validate("message", message)) {
	    alert("Message too short/long.");
	    return;
    }
	if(socketio) {
		if (type === 'public') {
			socketio.emit('message', message);
			document.getElementById("message").value = "";
		} else if (type === 'private') {
			const username = currentChat.name;
			socketio.emit('private-message', username, message);
			document.getElementById("message").value = "";
		} else if (type === 'group') {
			const groupname = currentChat.name;
			socketio.emit('group-message', groupname, message);
			document.getElementById("message").value = "";
		}
	}
}

function updateUserList(users) {
	if (!users) {
		return;
	}

	document.getElementById('allUsersList').innerHTML = "";
	userList = [];
	if (users && users.length > 0) {
		userList = users;
		userList.forEach(username => {
			username = sanitizeHTML(username);
			document.getElementById('allUsersList').innerHTML += `<p class="name" id="name" onclick="updateChatWindow('private', '${username}');">${username}</p>`;
		});
		// updateMessageType();
	}
}

function updateGroupList(groups) {
	if (!groups) {
		return;
	}

	document.getElementById('allGroupsList').innerHTML = "";
	groupList = [];
	if (groups && groups.length > 0) {
		groupList = groups;
		groupList.forEach(groupname => {
			groupname = sanitizeHTML(groupname);
			document.getElementById('allGroupsList').innerHTML += `<p class="name" onclick="updateChatWindow('group', '${groupname}');">${groupname}</p>`;
		});
		// updateMessageType();
	}
}

function updateFriendsList(friends) {
    if (!friends) {
        return;
    }
    document.getElementById('allFriendsList').innerHTML = "";
    friendsList = [];
    if (friends && friends.length > 0) {
        friendsList = friends;
        friendsList.forEach(friend => {
            document.getElementById('allFriendsList').innerHTML += `<p class="name" onclick="updateChatWindow('private', '${friend}');">${friend}</p>`;
        })
    }
}

function updateChatWindow(type, name) {
	// TODO: use id instead of name
	if (!checkChatWindow(type, name)) {
		return;
	}

	// console.log("DEBUG> Updating chat window", type, name);
	// Hide current Chat UI
	$(`#messages-${currentChat.type}-${currentChat.name}`).hide();
    $(`#messages.typing-${currentChat.type}-${currentChat.name}`).hide();
    $(`#messages.seen-${currentChat.type}-${currentChat.name}`).hide();

    // Display new Chat UI
    currentChat = {type, name};
	$('#chatName')[0].innerHTML = name;
    $(`#messages-${type}-${name}`).show();
    $(`#messages.typing-${type}-${name}`).hide();
    $(`#messages.seen-${type}-${name}`).hide();    
    // console.log("chat_history", type, name);
    // socketio.emit("chat_history", {type:type, name:name});
}

function checkChatWindow(type, name) {
	if (!type || !name || type.trim().length === 0 || name.trim().length === 0) {
		return false;
	}
	// TODO: username + groupname validation
	let element = $(`#messages-${type}-${name}`);
	if (element.length <= 0) {
		try {
			$('#messages')[0].innerHTML += `<div id="messages-${type}-${name}">
                    <div id="messages.typing-${type}-${name}" class="currentlyTyping position-absolute ms-2"></div>
                    <div id="messages.seen-${type}-${name}" class="seenStatus position-absolute"></div>
				</div>`;
            socketio.emit("chat_history", {type:type, name:name});
        } catch (e) {
			alert("There's a problem creating new chat UI.");
			return false;
		}
	}
	return true;
}

function updateMessageType() {
	const type = document.getElementById("messageTypeSelection").value;
	const input = document.getElementById("sendMessageName");

	if(type === "public") {
		input.style.display = "none";
	} else if (type === "private") {
		input.style.display = "block";
		autocompleteData = [];
		userList.forEach((user => autocompleteData.push({label: user, value: user})));
	} else if (type === "group") {
		input.style.display = "block";
		autocompleteData = [];
		groupList.forEach((name => autocompleteData.push({label: name, value: name})));
	}
}

function sendSeenStatus() {
	const seenStatus = status.seen[`${currentChat.type}-${currentChat.name}`];
	if(socketio && !seenStatus && status.authenticated) {
		// console.log("DEBUG> sendSeenStatus", currentChat.type, currentChat.name);
		socketio.emit('seen', currentChat.type, currentChat.name);
		status.seen[`${currentChat.type}-${currentChat.name}`] = true;
	}
}

function setSeenStatus(type, name, text) {
	if (text) {
		document.getElementById(`messages.seen-${type}-${name}`).innerHTML = "seen by " + text;
	} else {
		document.getElementById(`messages.seen-${type}-${name}`).innerHTML = "";
	}
}

function sendTyping() {
	socketio.emit("<TYPE>", currentChat.type, currentChat.name);
	// console.log("Sent to server: <TYPE>");
}

function checkEnter(event) {
	// console.log("DEBUG> checkEnter", event.key);
	if (event.key === "Enter") sendMessage();
}

function populateNewGroupNames() {
	const list = document.getElementById('newGroupMembers');
	list.innerHTML = "";
	if (friendsList) {
		friendsList.forEach((username) => {
			if (username != authenticatedUsername)
				list.innerHTML += '<li class="list-group-item"><input class="form-check-input me-1" type="checkbox" value="' + username + '">'+ sanitizeHTML(username) + '</li>';
		});
		$('#createGroupModal').modal('show');
	} else {
		alert("No user found.");
	}
}

function createNewGroup() {
	const groupName = document.getElementById('newGroupName');
	if (!groupName.validity.valid || !validate("groupname", groupName.value)) {
		alert('Invalid group name. Groupname must be 4 or more characters and contains no special characters.');
		return;
	}
	
	if (!authenticatedUsername) {
		alert('Invalid session. Please login.');
		return;
	}

	const groupMemberList = [authenticatedUsername];
	const list = document.getElementById('newGroupMembers').getElementsByClassName('form-check-input');
	for (const member of list) {
		if (member.checked && member.value) {
			groupMemberList.push(member.value);
		}
	}

	if (groupMemberList.length < 2) {
		alert('Group must have at least 2 members.');
		return;
	}

	if (socketio && groupMemberList.length > 1) {
		socketio.emit("create-group", groupName.value, groupMemberList);
		groupName.value = "";
		$('#createGroupModal').modal('hide');
	}
}

function register() {
    var username = document.getElementById('newusername').value;
    var password = document.getElementById('newpassword').value;
	if (!validate("username", username)) {
		document.getElementById("newusername").classList.add("is-invalid");
		document.getElementById("registration-error").innerHTML = "Invalid username. Must be at least 4 characters.";
		return;
	}
	if (!validate("password", password)) {
		document.getElementById("newpassword").classList.add("is-invalid");
		document.getElementById("registration-error").innerHTML = "Invalid password. Must be at least 6 characters and contain 1 digit, 1 lowercase, and 1 uppercase.";
		return;
	}
	if (socketio) {
		socketio.emit("register", username, password);
	}
}

function showRegistrationUI(toggle) {
	if (toggle) {
		$("#loginUI").hide();
		$("#registrationUI").show();
		$("#username")[0].value = "";
		$("#password")[0].value = "";
	} else {
		$("#loginUI").show();
		$("#registrationUI").hide();
		$("#newusername")[0].value = "";
		$("#newpassword")[0].value = "";
	}
}

function validate(type, value) {
	if (type === "username") {
		return (value && value.length >= 4 && value.length <= 100 && /^[a-zA-Z0-9]+$/.test(value));
	} else if (type === "password") {
		return /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value);
	} else if (type === "groupname") {
        return (value && value.length >= 4 && value.length <= 100 && /^[a-zA-Z0-9]+$/.test(value));
    }  else if (type === "message") {
        return (value && value.length >= 1 && value.length <= 500);
    }
}


function rightClick(e) {
    var sendFriendRequestBtn = document.getElementById("sendFriendRequestBtn");
    if(document.getElementById("contextMenu").style.display == "block") {
        hideMenu();
    } else if ($('.name:hover').length != 0) {
        // Only prevent default menu when hovering over name
        e.preventDefault();
        
        document.oncontextmenu = rightClick;
        var menu = document.getElementById("contextMenu")
        
        // Send Friend Request to user that you are hovering over
        $('.name').hover(function() {
            var userName = $(this).html();
            sendFriendRequestBtn.onclick = function() {sendFriendRequest(userName)};
        });
        
        menu.style.display = 'block';
        menu.style.left = e.pageX + "px";
        menu.style.top = e.pageY + "px";
    }
    // return false;
}

function sendFriendRequest(friend) {
    if (friend == authenticatedUsername) {
        
        socketio.emit("friendRequest", "self", authenticatedUsername, friend);
    } else if (friendsList.includes(friend)) {
        socketio.emit("friendRequest", "isFriend", authenticatedUsername, friend);
    } else {
        socketio.emit("friendRequest", "notFriend", authenticatedUsername, friend);
    }
}

socketio.on("friendRequestNotif", (data) => {
    const sender = data.senderUsername;
    const friend = data.recieverUsername;

    var acceptFriendRequestBtn = document.getElementById("acceptFriRequestBtn");
    var toastText = document.getElementById("toastText");
    var friendRequestToast = new bootstrap.Toast(document.getElementById("friendRequestToast"));
    var friendRequestToastBody = document.getElementById("friendRequestToastBody");
    toastText.innerHTML = "Hey! " + sender + " wants to be friends!";

    
    $("#acceptFriRequestBtn").click(function() {
        acceptFriendRequest(sender, friend, friendRequestToast);
      });
    friendRequestToast.show();
});

socketio.on("alreadyFriendNotif", (data) => {
    
    const sender = data.senderUsername;
    const friend = data.recieverUsername;

    var alreadyFriendToastText = document.getElementById("alreadyFriendToastText");
    var alreadyFriendToast = new bootstrap.Toast(document.getElementById("alreadyFriendToast"));
    alreadyFriendToastText.innerHTML = "You are already friends with " + friend + "!";
    alreadyFriendToast.show();
});

function acceptFriendRequest(sender, reciever) {
    socketio.emit("acceptFriendRequest", sender, reciever);
}

socketio.on("updateFriendsList", (friendsList) => {
    updateFriendsList(friendsList);
});

function setRightClickMenu() {
    document.oncontextmenu = rightClick;
}
function hideMenu() {
    document.getElementById("contextMenu").style.display = "none"
}

/**
 * Sanitize input HTML strings.
 * @param {string} text
 * @return {string}
 */
function sanitizeHTML(text) {
	return $('<div>').text(text).html();
}
/*window.addEventListener("load", () => {
	const input = document.getElementById("sendMessageName");
	autocomplete({
		input: input,
		minLength: 1,
		fetch: (text, update) => {
			text = text.toLowerCase();
			console.log(autocompleteData);
			// you can also use AJAX requests instead of preloaded data
			const filterData = autocompleteData.filter(n => {
				if (authenticatedUsername && n.label === authenticatedUsername) {
					return false;
				}
				return n.label.toLowerCase().match(text)
			});
			update(filterData);
		},
		onSelect: item =>  input.value = item.label,
		className: 'list-group-item autocomplete'
	});
});*/
